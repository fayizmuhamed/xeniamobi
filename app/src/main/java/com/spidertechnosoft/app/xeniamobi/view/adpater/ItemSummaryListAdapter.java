package com.spidertechnosoft.app.xeniamobi.view.adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spidertechnosoft.app.xeniamobi.R;
import com.spidertechnosoft.app.xeniamobi.model.dictionary.ItemSummary;
import com.spidertechnosoft.app.xeniamobi.model.helper.GeneralMethods;

import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class ItemSummaryListAdapter extends BaseSwipListAdapter {

    private final Context context;
    private List<ItemSummary> itemSummaries = null;
    private LayoutInflater mInflater;

    public ItemSummaryListAdapter(@NonNull Context context, @NonNull List<ItemSummary> data) {
        this.context=context;

        this.itemSummaries=data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemSummaries.size();
    }

    @Override
    public ItemSummary getItem(int position) {
        return itemSummaries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView( final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.lay_item_summary_list_item, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        final ItemSummary itemSummary=itemSummaries.get(position);

        if(itemSummary!=null){

            holder.lblItemSummaryName.setText(itemSummary.getItemName());
            holder.lblItemSummaryPrice.setText(GeneralMethods.formatNumber(itemSummary.getItemPrice()));
            holder.lblItemSummaryQty.setText(itemSummary.getItemQty().toString());
            holder.lblItemSummaryAmount.setText(GeneralMethods.formatNumber(itemSummary.getItemAmount()));

        }

        return convertView;


    }

    @Override
    public boolean getSwipeEnableByPosition(int position) {
        if(position % 2 == 0){
            return false;
        }
        return true;
    }

    class ViewHolder {
        TextView lblItemSummaryName;

        TextView lblItemSummaryPrice;

        TextView lblItemSummaryQty;

        TextView lblItemSummaryAmount;


        public ViewHolder(View rowView) {
             lblItemSummaryName=(TextView) rowView.findViewById(R.id.lblItemSummaryName);

             lblItemSummaryPrice=(TextView) rowView.findViewById(R.id.lblItemSummaryPrice);

             lblItemSummaryQty=(TextView) rowView.findViewById(R.id.lblItemSummaryQty);

             lblItemSummaryAmount=(TextView) rowView.findViewById(R.id.lblItemSummaryAmount);

            rowView.setTag(this);
        }
    }


}
