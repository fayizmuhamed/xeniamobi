package com.spidertechnosoft.app.xeniamobi.view.helper;

public enum OperationFragments {
    STAFF_FRAGMENT,
    PRODUCT_FRAGMENT,
    DETAIL_FRAGMENT,
    CASH_DESK
}
