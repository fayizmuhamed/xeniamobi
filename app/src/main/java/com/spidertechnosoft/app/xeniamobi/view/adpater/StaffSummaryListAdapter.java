package com.spidertechnosoft.app.xeniamobi.view.adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spidertechnosoft.app.xeniamobi.R;
import com.spidertechnosoft.app.xeniamobi.model.dictionary.StaffSummary;
import com.spidertechnosoft.app.xeniamobi.model.helper.GeneralMethods;

import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class StaffSummaryListAdapter extends BaseSwipListAdapter {

    private final Context context;
    private List<StaffSummary> waiterSummaries = null;
    private LayoutInflater mInflater;


    public StaffSummaryListAdapter(@NonNull Context context, @NonNull List<StaffSummary> data) {
        this.context=context;

        this.waiterSummaries=data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return waiterSummaries.size();
    }

    @Override
    public StaffSummary getItem(int position) {
        return waiterSummaries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView( final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.lay_waiter_summary_list_item, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        final StaffSummary staffSummary =waiterSummaries.get(position);

        if(staffSummary !=null){

            holder.lblWaiterSummaryName.setText(staffSummary.getUserName());
            holder.lblWaiterSummaryAmount.setText(GeneralMethods.formatNumber(staffSummary.getAmount()));

        }



        return convertView;


    }

    @Override
    public boolean getSwipeEnableByPosition(int position) {
        if(position % 2 == 0){
            return false;
        }
        return true;
    }

    class ViewHolder {
        TextView lblWaiterSummaryName;

        TextView lblWaiterSummaryAmount;


        public ViewHolder(View rowView) {
             lblWaiterSummaryName=(TextView) rowView.findViewById(R.id.lblWaiterSummaryName);

             lblWaiterSummaryAmount=(TextView) rowView.findViewById(R.id.lblWaiterSummaryAmount);


            rowView.setTag(this);
        }
    }


}

