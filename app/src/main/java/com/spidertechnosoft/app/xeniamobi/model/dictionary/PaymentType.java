package com.spidertechnosoft.app.xeniamobi.model.dictionary;

public class PaymentType {
    public static final int CASH = 0;
    public static final int CARD = 1;
    public static final int CREDIT = 2;
    public static final int BOTH = 3;

    public static String getPaymentTypeDesc(int paymentType){

        String paymentTypeDesc="";
        switch (paymentType){

            case CASH: paymentTypeDesc="CASH";
                break;
            case CARD: paymentTypeDesc="CARD";
                break;
            case CREDIT: paymentTypeDesc="CREDIT";
                break;
            case BOTH: paymentTypeDesc="BOTH";
                break;
            default:paymentTypeDesc="";
        }

        return paymentTypeDesc;
    }
}
