package com.spidertechnosoft.app.xeniamobi.model.dictionary;

import java.util.HashMap;
import java.util.Map;

public enum PrinterModel {

    OTHER(0),
    CIONTEK(1),
    SUNMI_V1(2),
    RP_80(3);

    private int value;
    private static Map map = new HashMap<>();

    private PrinterModel(int value) {
        this.value = value;
    }

    static {
        for (PrinterModel printerModel : PrinterModel.values()) {
            map.put(printerModel.value, printerModel);
        }
    }

    public static PrinterModel valueOf(int deviceType) {
        return (PrinterModel) map.get(deviceType);
    }
}
