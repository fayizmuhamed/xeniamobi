package com.spidertechnosoft.app.xeniamobi.view.adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spidertechnosoft.app.xeniamobi.R;
import com.spidertechnosoft.app.xeniamobi.model.dictionary.CategorySummary;
import com.spidertechnosoft.app.xeniamobi.model.helper.GeneralMethods;

import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class CategorySummaryListAdapter extends BaseSwipListAdapter {

    private final Context context;
    private List<CategorySummary> categorySummaries = null;
    private LayoutInflater mInflater;

    public CategorySummaryListAdapter(@NonNull Context context, @NonNull List<CategorySummary> data) {
        this.context=context;

        this.categorySummaries=data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categorySummaries.size();
    }

    @Override
    public CategorySummary getItem(int position) {
        return categorySummaries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView( final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.lay_category_summary_list_item, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        final CategorySummary categorySummary=categorySummaries.get(position);

        if(categorySummary!=null){

            holder.lblCategorySummaryName.setText(categorySummary.getCategoryName());
            holder.lblCategorySummaryQty.setText(categorySummary.getCategoryQty().toString());
            holder.lblCategorySummaryAmount.setText(GeneralMethods.formatNumber(categorySummary.getCategoryAmount()));

        }

        return convertView;


    }

    @Override
    public boolean getSwipeEnableByPosition(int position) {
        if(position % 2 == 0){
            return false;
        }
        return true;
    }

    class ViewHolder {
        TextView lblCategorySummaryName;

        TextView lblCategorySummaryQty;

        TextView lblCategorySummaryAmount;


        public ViewHolder(View rowView) {
             lblCategorySummaryName=(TextView) rowView.findViewById(R.id.lblCategorySummaryName);

             lblCategorySummaryQty=(TextView) rowView.findViewById(R.id.lblCategorySummaryQty);

             lblCategorySummaryAmount=(TextView) rowView.findViewById(R.id.lblCategorySummaryAmount);


            rowView.setTag(this);
        }
    }


}
