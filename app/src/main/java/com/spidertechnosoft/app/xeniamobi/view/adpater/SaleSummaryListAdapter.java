package com.spidertechnosoft.app.xeniamobi.view.adpater;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spidertechnosoft.app.xeniamobi.R;
import com.spidertechnosoft.app.xeniamobi.model.domain.Sale;
import com.spidertechnosoft.app.xeniamobi.model.helper.GeneralMethods;

import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class SaleSummaryListAdapter extends BaseSwipListAdapter {

    private final Context context;
    private List<Sale> sales = null;
    private LayoutInflater mInflater;


    public SaleSummaryListAdapter(@NonNull Context context, @NonNull List<Sale> data) {

        this.context=context;
        this.sales=data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sales.size();
    }

    @Override
    public Sale getItem(int position) {
        return sales.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView( final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.lay_sale_report_list_item, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        final Sale sale=sales.get(position);

        if(sale!=null){

            holder.lblSaleReportItemInvoiceDate.setText(GeneralMethods.convertDateFormat(sale.getSalesDate(),GeneralMethods.SERVER_DATE_TIME_FORMAT,GeneralMethods.LOCAL_DATE_TIME_FORMAT));
            holder.lblSaleReportItemInvoiceNo.setText(sale.getId().toString());
            holder.lblSaleReportItemAmount.setText(GeneralMethods.formatNumber(sale.getNetAmount()));

        }


        return convertView;


    }

    @Override
    public boolean getSwipeEnableByPosition(int position) {
        if(position % 2 == 0){
            return false;
        }
        return true;
    }

    class ViewHolder {
        TextView lblSaleReportItemInvoiceDate;

        TextView lblSaleReportItemInvoiceNo;

        TextView lblSaleReportItemAmount;



        public ViewHolder(View rowView) {
            lblSaleReportItemInvoiceDate=(TextView) rowView.findViewById(R.id.lblSaleReportItemInvoiceDate);

             lblSaleReportItemInvoiceNo=(TextView) rowView.findViewById(R.id.lblSaleReportItemInvoiceNo);

             lblSaleReportItemAmount=(TextView) rowView.findViewById(R.id.lblSaleReportItemAmount);


            rowView.setTag(this);
        }
    }


}
